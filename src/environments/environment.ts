

export const environment = {
  production: false,
  /* ---- locale ----- */
  baseUrl: 'http://localhost:8080/api',
  pointing: "local",


  /* ----  test ----- */
  // baseUrl: 'https://pro-equipped-osprey.ngrok-free.app/api',
  // pointing: 'test',

  /* ----  mock ----- */
  // baseUrl: 'http://localhost:3000/api',
  // pointing: 'test',

  /* ---- produzione ----- * /
  baseUrl: '',
  pointing: "production",
   */

};
