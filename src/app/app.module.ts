import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {AkitaNgDevtools} from "@datorama/akita-ngdevtools";
import {EventsService} from "./services/events.service";
import {HomeAdminPageModule} from "./page/tabs/page/admin/home-admin/home-admin.module";
import {HttpService} from "./services/http.service";
import {SessionService} from "./services/session.service";
import { EditModalComponent } from './component/edit-modal/edit-modal.component';
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
    declarations: [AppComponent, EditModalComponent],
    imports: [
      BrowserModule,
      AkitaNgDevtools.forRoot(),
      HttpClientModule,
      IonicModule.forRoot(),
      AppRoutingModule,
      HomeAdminPageModule,
      FormsModule,
      BrowserAnimationsModule
      ],
    providers: [
        EventsService,
        {
            provide: HTTP_INTERCEPTORS, useClass: HttpService, multi: true
        },
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        {
            provide: APP_INITIALIZER,
            deps: [SessionService],
            useFactory: function (session: SessionService): () => Promise<boolean> {
                return () => {
                    return new Promise((resolve, reject) => {

                        const allowedUrls = ['/login'];

                        const user = session.loggedUser()
                        if (user.logged && user.admin) {
                            allowedUrls.push('/tabs/home-admin');
                        }else if(user.logged){
                            allowedUrls.push('/tabs/home');
                        }

                        const startUrl = window.location.pathname;
                        console.log(startUrl)
                        if (!allowedUrls.includes(startUrl)) {
                            location.replace(allowedUrls[0]);
                            reject(false);
                        }else{
                          resolve(true);
                        }
                    });
                };

            },
            multi: true
        },
    ],
    exports: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
