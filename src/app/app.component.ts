import {Component, OnInit} from '@angular/core';
import {AppService} from "./services/app.service";
import {SessionService} from "./services/session.service";
import {User} from "./component/common/interface";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent{
  username = 'alex';
  password = 'test';
  rememberMe = true

  user: User = {};
  admin = false;

  constructor(private appService: AppService, private sessionService: SessionService, private router: Router) {
    // this.sessionService.login(this.username, this.password, this.rememberMe).then(() => {
    //   this.user = this.sessionService.getStore().getValue().user
    //   this.admin = this.sessionService.getStore().getValue().admin
    //
    //   if(this.admin){
    //     this.router.navigate(['tabs/home-admin'])
    //   }
    // });
  }

}
