import { Component } from '@angular/core';
import {IonicModule} from "@ionic/angular";

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
  imports: [
    IonicModule
  ],
  standalone: true
})
export class SpinnerComponent  {

  constructor() { }

}
