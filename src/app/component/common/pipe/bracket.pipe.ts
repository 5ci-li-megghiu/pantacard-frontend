import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addBrackets',
})
export class AddBracketsPipe implements PipeTransform {

  transform(value: string, ): string {
    return `{${value}}`
  }

}
