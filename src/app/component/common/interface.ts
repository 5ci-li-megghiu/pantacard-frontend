import {ID} from "@datorama/akita";

export interface User {
  id?: ID,
  nome?: string,
  cognome?: string,
  email?: string,
  data?: string,
  ruolo?: string,
  cellulare?: string,
  username?: string,
  codice?: string,
}

export interface Risposta {
  code: number;
  title: string;
  titolo: string;
  message: string;
  data?: any;
}

export interface Fidelity {
  id?: ID,
  dataInizio?: string,
  dataFine?: string,
  totPunti?: number,
  punti?: punto[],
}

export interface punto{
  ts: string,
  id: ID,
  importo: number,
  note?: string,
}
