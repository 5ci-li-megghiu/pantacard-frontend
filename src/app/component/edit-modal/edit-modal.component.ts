import {Component, Input, OnInit} from '@angular/core';
import { ModalController } from '@ionic/angular';
import {AlertService} from "../../services/alert.service";

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss'],
})
export class EditModalComponent  implements OnInit {
  @Input() point: number = 0;
  _point: number = 0;

  constructor(private modalController: ModalController, private alertService: AlertService){
  }

  ngOnInit() {
    this._point = this.point;
    console.log(this.point);
  }

  dismiss() {
    if(this._point !== this.point){
      this.alertService.presentPopup('Attenzione', 'Sei sicuro di voler modificare i punti dell\'utente?', [
        {
          text: 'Annulla',
        },
        {
          text: 'Conferma',
          handler: () => {
            this.modalController.dismiss(this.point).then();
          }
        }
      ]).then();
    }else{
      this.modalController.dismiss(false).then();
    }

  }

}
