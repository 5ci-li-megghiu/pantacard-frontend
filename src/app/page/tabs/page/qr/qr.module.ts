import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrPageRoutingModule } from './qr-routing.module';

import { QrPage } from './qr.page';
import {SpinnerComponent} from "../../../../component/common/spinner/spinner.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        QrPageRoutingModule,
        SpinnerComponent
    ],
  declarations: [QrPage]
})
export class QrPageModule {}
