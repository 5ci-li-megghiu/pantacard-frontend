import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs/internal/Observable";
import {AppService} from "../../../../services/app.service";
import {ModalController} from "@ionic/angular";
import {FidelityService} from "../../../../services/fidelity.service";

@Component({
  selector: 'app-qr',
  templateUrl: './qr.page.html',
  styleUrls: ['./qr.page.scss'],
})
export class QrPage implements OnInit {

  screenWidth = window.innerWidth;
  $loading: Observable<boolean>;
  isLoading = false;
  @Input() codice: string = '';

  constructor(private appService: AppService, private modalCtrl: ModalController, private fidelityService: FidelityService) {
    this.$loading = this.appService.getQuery().select('rqActive')
  }

  ngOnInit() {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.screenWidth = window.innerWidth;
  }

  close(){
    this.modalCtrl.dismiss()
  }

  getQrCode() {
    return `https://api.qrserver.com/v1/create-qr-code/?data=${this.codice}&size=360x360&margin=30`
  }
}
