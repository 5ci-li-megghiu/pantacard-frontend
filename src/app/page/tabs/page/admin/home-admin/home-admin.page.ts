import {Component, OnDestroy, OnInit} from '@angular/core';
import { ModalController } from '@ionic/angular';
import {EditModalComponent} from "../../../../../component/edit-modal/edit-modal.component";
import {FidelityService} from "../../../../../services/fidelity.service";
import {User} from "../../../../../component/common/interface";
import {AlertService} from "../../../../../services/alert.service";
import {ID} from "@datorama/akita";
import {AppService} from "../../../../../services/app.service";
import {Observable} from "rxjs/internal/Observable";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.page.html',
  styleUrls: ['./home-admin.page.scss'],
})
export class HomeAdminPage implements OnInit , OnDestroy{

  list: User[]= []
  unFilteredList: User[] = [];
  $loading: Observable<boolean>;
  sub?: Subscription;
  isLoading: boolean = false;

  constructor(private modalController: ModalController, private fidelityService: FidelityService, private alertService: AlertService,
              private appService: AppService) {
    this.$loading = this.appService.getQuery().select('rqActive')
  }

  ngOnInit() {
    this.sub = this.$loading.subscribe((loading) => this.isLoading = loading);
    this.fidelityService.getUserList().then((list) => {
      this.list = [...list];
      this.unFilteredList = [...list];
    }).catch((err) => {
      console.error(err);
      this.alertService.presentToast('Errore', 'Errore nel caricamento della lista utenti', 'danger');
    });
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }

  filterItems(event: any) {
    const searchTerm = event.target.value.toLowerCase();
    if(searchTerm){
      let filteredItems = this.list.filter(item =>
        item.username!.toLowerCase().includes(searchTerm)
      );
      this.list = [...filteredItems];
    }else{
      this.list = [...this.unFilteredList];
    }
  }

  openModal(){
    this.modalController.create({
      component: EditModalComponent
    }).then((modal) => {
      modal.present()
    } );
  };

  async editPoint(user: User) {
    this.fidelityService.getPointByCodice(user.codice!).then(async (point) => {
      console.log(point);
      const modal = await this.modalController.create({
        component: EditModalComponent,
        componentProps: {point: point.n_punti},
        breakpoints: [0.4, 0.5],
        initialBreakpoint: 0.4,
        backdropDismiss: false,
        showBackdrop: true,
        canDismiss: true,
        handle: true,
        handleBehavior: 'cycle',
      })

      await modal.present();
      await modal.onWillDismiss().then((modal) => {
        console.log(modal.data);
        if (modal && modal.data){
          this.fidelityService.modifyPoint(user.id!, modal.data).then((res: any) => {
            if (res && res.totPunti === modal.data){
              this.alertService.presentToast('Successo', res.message, 'success');
            }
          }).catch((err: any) => {
            console.error(err);
            this.alertService.presentToast('Errore', 'Errore nella modifica dei punti', 'danger');
          });
        }else{
          this.alertService.presentToast('Attenzione', 'Operazione annullata', 'warning');
        }
      });

    }).catch((err) => {
      console.error(err);
      this.alertService.presentToast('Errore', 'Errore nella lettura dei punti', 'danger');
    });
  }
}
