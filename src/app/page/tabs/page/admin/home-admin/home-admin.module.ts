import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAdminPageRoutingModule } from './home-admin-routing.module';

import { HomeAdminPage } from './home-admin.page';
import {SpinnerComponent} from "../../../../../component/common/spinner/spinner.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomeAdminPageRoutingModule,
        SpinnerComponent
    ],
    exports: [
        HomeAdminPage
    ],
    declarations: [HomeAdminPage]
})
export class HomeAdminPageModule {}
