import { Component, OnInit } from '@angular/core';
import {ModalController, Platform} from "@ionic/angular";
import {Barcode, BarcodeScanner} from "@capacitor-mlkit/barcode-scanning";
import {AlertService} from "../../../../services/alert.service";
import {AppService} from "../../../../services/app.service";
import {FidelityService} from "../../../../services/fidelity.service";

@Component({
  selector: 'app-scan',
  templateUrl: './scan.page.html',
  styleUrls: ['./scan.page.scss'],
})
export class ScanPage implements OnInit {
  isSupported = false;
  isMobile = false;
  device: string = ''
  fase: number = 1;
  punti: number = 0;
  codice: string = '';
  spesa: number = 0;

  temp = {"format":"QR_CODE","displayValue":"69wasd888","cornerPoints":[],"valueType":"TEXT","rawValue":"69wasd888","bytes":[54,57,119,97,115,100,56,56,56]}

  constructor(private modalCtrl: ModalController, private alertService: AlertService, private fidelityService: FidelityService,
              private platform: Platform, private appService: AppService) {
    this.device = this.appService.getStore().getValue().deviceInfo!.os;
  }
  async ngOnInit() {
    if (this.platform.is('capacitor') || this.platform.is('cordova')) {
      if (this.device.toLowerCase() === 'android') {
        this.isSupported = await this.checkAndroidSupport();
      } else if (this.device.toLowerCase() === 'ios') {
        this.isSupported = await this.checkIosSupport();
      }else {
        this.alertService.presentPopup('Attenzione', 'Dispositivo mobile non riconosciuto', [{text: 'OK'}]).then();
      }
    } else {
      this.alertService.presentPopup('Attenzione', 'Scansione disponibile solo su mobile', [{text: 'OK'}]).then();
    }
  }

  async checkAndroidSupport(): Promise<boolean> {
    const service = await this.googleService();
    if (service) {
      const result = await BarcodeScanner.isSupported();
      this.isMobile = true;
      console.log('BarcodeScanner isSupported', result.supported);
      return result.supported;
    } else {
      this.alertService.presentPopup('Attenzione', 'Scansione Qrcode non disponibile su questo dispositivo', [{text: 'OK'}]).then();
      return false;
    }
  }

  async checkIosSupport(): Promise<boolean> {
    const result = await BarcodeScanner.isSupported();
    this.isMobile = true;
    console.log('BarcodeScanner isSupported', result.supported);
    return result.supported;
  }


  close(){
    this.modalCtrl.dismiss()
  }

  async scan(): Promise<void> {
    const granted = await this.requestPermissions();
    if (!granted) {
      this.alertService.presentPopup('Permission Denied',
        'Camera permission is required to scan barcodes.',
        [{text: 'OK'}]).then();
      return;
    }

    const { barcodes } = await BarcodeScanner.scan();
    this.codice = this.decryptBarcode(barcodes[0]);
    this.submitCodice();
  }

  submitCodice(){
    this.fidelityService.getPointByCodice(this.codice).then(punti => {
      if(!!punti.n_punti || punti.n_punti === 0){
        if(punti.n_punti < 12){
          this.fase = 2;
          this.punti = punti.n_punti;
        }else{
          this.punti = punti.n_punti;
          this.alertService.presentPopup('Attenzione',
            'Il cliente ha raggiunto il massimo dei punti e ha diritto a uno sconto del 10%',
            [
              {
                text: 'OK',
                handler: () => {
                  this.fidelityService.resetFidelity(this.codice).then((res: any) => {
                    this.alertService.presentToast('Successo','Fidelity completata con successo', 'success').then();
                    this.modalCtrl.dismiss().then();
                  }).catch((err) => this.errorHandler('Errore nel completamento della fidelity', err));
                }
              }
            ]).then();
        }
      }else{
        this.alertService.presentToast('Errore','Codice non valido', 'danger').then();
      }
    }).catch((err) => this.errorHandler('Errore nel recupero dei punti', err));
  }

  aggiungiPunto(){
    this.fidelityService.aggiungiPunto(this.codice, (this.punti + 1), this.spesa).then((res: any) => {
      if(res.code === 0){
        this.alertService.presentToast('Successo','Punto aggiunto con successo', 'success').then();
        this.modalCtrl.dismiss().then();
      }else{
        this.errorHandler(res.message);
      }
    }).catch((err) => this.errorHandler('Errore nel aggiunta dei punti', err));
  }

  async requestPermissions(): Promise<boolean> {
   await BarcodeScanner.checkPermissions()
    const { camera } = await BarcodeScanner.requestPermissions();
    return camera === 'granted' || camera === 'limited';
  }

  decryptBarcode(barcode: Barcode): string {
    if(barcode && barcode.rawValue){
      return barcode.rawValue;
    }
    return ''
  }

  async googleService(): Promise<boolean> {
    const google = await BarcodeScanner.isGoogleBarcodeScannerModuleAvailable();
    if (google.available) {
      return true;
    } else {
      await BarcodeScanner.installGoogleBarcodeScannerModule();
      return false;
    }
  }

  codeInput(event: any){
    this.codice = event.detail.value;
  }

  errorHandler(message: string, err?: any){
    this.alertService.presentToast('Errore', message, 'danger').then();
    if (err) {
      console.log(err);
    }
  }

  spesaInput(event: any) {
    this.spesa = + event.detail.value;
  }
}
