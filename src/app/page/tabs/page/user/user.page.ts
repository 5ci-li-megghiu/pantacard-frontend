import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {SessionService} from "../../../../services/session.service";
import {Router} from "@angular/router";
import {User} from "../../../../component/common/interface";
import {Observable} from "rxjs/internal/Observable";
import {AppService} from "../../../../services/app.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit, OnDestroy{

  user: User = {};
  admin = false;
  $loading: Observable<boolean>;
  subscription: Subscription[] = [];
  isLoading = false;


  constructor(private sessionService: SessionService, private router : Router, private appService: AppService) {
    this.$loading = this.appService.getQuery().select('rqActive')
  }

  ngOnInit() {
    this.subscription[0] = this.sessionService.getQuery().user$.subscribe(user => this.user = user!)
    this.subscription[1] = this.sessionService.getQuery().isAdmin$.subscribe(admin => this.admin = admin)
  }

  logout() {
    this.sessionService.logout();

    this.router.navigate(['login']);
  }

  ngOnDestroy(): void {
   this.subscription.forEach(sub => sub.unsubscribe())
  }

}
