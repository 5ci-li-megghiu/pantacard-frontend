import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {SessionService} from '../../../../services/session.service';
import {Fidelity, User} from "../../../../component/common/interface";
import {FidelityService} from "../../../../services/fidelity.service";
import {firstValueFrom, lastValueFrom, Subscription} from "rxjs";
import {Observable} from "rxjs/internal/Observable";
import {AppService} from "../../../../services/app.service";
import {AlertService} from "../../../../services/alert.service";
import * as moment from "moment";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePage implements OnInit, OnDestroy{

  user: User = {};
  admin = false;
  screenWidth = window.innerWidth;
  fidelity?: Fidelity;
  $loading: Observable<boolean>;
  isLoading = false;
  fidelityOpen: boolean = false
  subscription: Subscription[] = [];

  constructor(private sessionService: SessionService, private alertService: AlertService,
              private FidelityService: FidelityService, private appService: AppService, private cdr: ChangeDetectorRef
  ) {
    this.$loading = this.appService.getQuery().select('rqActive')
    this.subscription[0] = this.sessionService.getQuery().user$.subscribe(user => {
      if (user){
        this.user = user
        this.FidelityService.getFidelity(user.id!)
      }
    })
    this.subscription[1] = this.sessionService.getQuery().isAdmin$.subscribe(admin => this.admin = admin)
    this.subscription[2] = this.FidelityService.getQuery().fidelity$.subscribe(fidelity => this.fidelity = fidelity)
  }

  ngOnInit(): void {
    this.$loading.subscribe((loading) => {
      this.isLoading = loading
      this.cdr.detectChanges();
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.screenWidth = window.innerWidth;
  }

  ngOnDestroy(): void {
    this.user = {};
    this.admin = false;
    this.fidelity = undefined;
    this.subscription.forEach(sub => sub.unsubscribe())
  }
}
