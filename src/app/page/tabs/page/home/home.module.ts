import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { AddBracketsPipe } from '../../../../component/common/pipe/bracket.pipe';
import {SpinnerComponent} from "../../../../component/common/spinner/spinner.component";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        NgOptimizedImage,
        SpinnerComponent
    ],
  declarations: [HomePage, AddBracketsPipe]
})
export class HomePageModule {}
