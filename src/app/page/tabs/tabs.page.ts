import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {SessionService} from "../../services/session.service";
import {User} from "../../component/common/interface";
import {Router} from "@angular/router";
import {ModalController, NavController} from "@ionic/angular";
import {QrPage} from "./page/qr/qr.page";
import {ScanPage} from "./page/scan/scan.page";
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsPage implements OnInit , OnDestroy{

  user: User = {};
  subscription: Subscription[] = [];
  admin = false;

  constructor(private sessionService: SessionService, private router: Router, private modalCtrl: ModalController){
    this.subscription[0] = this.sessionService.getQuery().user$.subscribe(user => {
      if (user){
        this.user = user;
      }
    })
    this.subscription[1] = this.sessionService.getQuery().isAdmin$.subscribe(admin => this.admin = admin)
  }

  ngOnInit(): void {
    if(!this.user){
      this.router.navigate(['login'])
    }
  }

  scan(type: 'user'| 'admin'){
    if(type === 'admin'){
      this.modalCtrl.create({
        component: ScanPage,
        showBackdrop: true,
      }).then((modal) => {
        modal.present()
      })
    }else{
      this.modalCtrl.create({
        component: QrPage,
        componentProps: {
          codice: this.user.codice
        },
        showBackdrop: true,
      }).then((modal) => {
        modal.present()
      })
    }
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe())
  }
}
