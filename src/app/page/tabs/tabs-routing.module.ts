import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';
import {AuthGuard} from "../../services/auth.guard";
import {AdminGuard} from "../../services/admin.guard";

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('./page/home/home.module').then( m => m.HomePageModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'home-admin',
        loadChildren: () => import('./page/admin/home-admin/home-admin.module').then( m => m.HomeAdminPageModule),
        canActivate: [AdminGuard]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      },
      {
        path: 'user',
        loadChildren: () => import('./page/user/user.module').then( m => m.UserPageModule)
      },
      {
        path: 'qr',
        loadChildren: () => import('./page/qr/qr.module').then( m => m.QrPageModule)
      },
      {
        path: 'scan',
        loadChildren: () => import('./page/scan/scan.module').then( m => m.ScanPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
