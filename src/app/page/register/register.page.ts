import { Component, OnInit } from '@angular/core';
import {FormBuilder, UntypedFormGroup, Validators} from "@angular/forms";
import {User} from "../../component/common/interface";
import {Router} from "@angular/router";
import {AlertService} from "../../services/alert.service";
import {SessionService} from "../../services/session.service";
import {AuthService} from "../../services/auth.service";
import * as moment from "moment";

export interface RegisterData {
  nome: string;
  cognome: string;
  email: string;
  data: string;
  cellulare: string;
  username: string;
  password: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  form: UntypedFormGroup;
  isLoading: boolean = false;

  user: User = {};
  admin = false;

  date = ''

  constructor(private fb: FormBuilder, private router: Router,private alertService: AlertService,
              private sessionService: SessionService, private auth: AuthService) {
    //nome,cognome, email, password, data, cellulare, username
    this.form = this.fb.group({
      nome: ['', Validators.required],
      cognome: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      data: [''],
      cellulare: ['' , Validators.pattern('^[0-9]*$')],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });

    this.user = this.sessionService.getStore().getValue().user
    this.admin = this.sessionService.getStore().getValue().admin
  }

  ngOnInit(): void {
  }

  submit(){
    let body = this.form.value as RegisterData;

    if(this.date){
      body.data = this.date;
    }

    this.isLoading = true;

    if (!!body){
      this.sessionService.signIn(body).then((resp) => {
        this.isLoading = false;
        if(resp){
          this.redirect();
        }
      }).catch(err => {
        console.log(err)
        this.isLoading = false;
      });
    }
  }

  redirect(){
    if(this.admin){
      this.router.navigate(['tabs/home-admin']).then()
    }else{
      this.router.navigate(['tabs/home']).then()
    }
  }

  selectDate(event: any){
    this.date = moment(event.detail.value).format('DD/MM/YYYY')
  }

  accedi() {
    this.router.navigate(['login']).then();
  }
}
