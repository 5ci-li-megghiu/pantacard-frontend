import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, UntypedFormGroup, Validators} from "@angular/forms";
import {NavController} from "@ionic/angular";
import {LoginData} from "../../store/auth/auth.store";
import {AuthService} from "../../services/auth.service";
import {firstValueFrom, timeout} from "rxjs";
import {SessionService} from "../../services/session.service";
import {User} from "../../component/common/interface";
import {Router} from "@angular/router";
import {AlertService} from "../../services/alert.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  form: UntypedFormGroup;
  isLoading: boolean = false;

  user: User = {};
  admin = false;

  constructor(private fb: FormBuilder, private router: Router,private alertService: AlertService,
              private sessionService: SessionService, private auth: AuthService) {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(3)]],
      rememberMe: [false]
    });

    this.user = this.sessionService.getStore().getValue().user
    this.admin = this.sessionService.getStore().getValue().admin
    let rememberMe = this.auth.getStore().getValue().rememberMe

    if(this.user && rememberMe){
      this.redirect();
    }
  }

  ngOnInit(): void {
  }

  submit(){
    const value = this.form.value as LoginData;

    this.isLoading = true;

    if (!!value){
      this.sessionService.login(value.username, value.password, value.rememberMe ? value.rememberMe : false)
        .then((resp) => {
          if(resp){
            console.log(resp)
            this.isLoading = false;
            if(resp.code === 200){
              this.user = this.sessionService.getStore().getValue().user
              this.admin = this.sessionService.getStore().getValue().admin

              this.redirect();
            }else if(resp.code === 401) {
              this.alertService.presentToast('Errore', 'Credenziali errate', 'danger').then();
              console.error('credenziali errate');
            }else if(resp.code === 500){
              this.alertService.presentToast('Errore', 'Errore interno al server', 'danger').then();
              console.error('errore interno al server');
            }
          }
        }).catch(err => {
          console.error(err);
          this.isLoading = false;
      });
    }
  }

  redirect(){
    setTimeout(() => {
      if(this.admin){
        this.router.navigate(['tabs/home-admin']).then()
      }else{
        this.router.navigate(['tabs/home']).then()
      }
    }, 200);
  }

  register(){
    this.router.navigate(['register']).then()
  }
}
