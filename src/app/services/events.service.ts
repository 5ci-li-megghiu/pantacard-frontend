import { BehaviorSubject } from 'rxjs';

export class EventsService {
    showToast$ = new BehaviorSubject<any>(null);
    showPopup$ = new BehaviorSubject<any>(null);


    public presentToast = (
        header: string,
        message: string,
        color = 'dark',
        position: 'top' | 'bottom' | 'middle' = 'top'
    ) => {
        const toast = {
            header,
            message,
            position,
            color,
            duration: 2000
        };

        this.showToast$.next({...toast});
    }

    public presentPopup = (header: string, message: string, buttons?: any[]) => {
        const toast = {
            header,
            message,
            buttons: buttons ? buttons : ['Ok'],
            backdropDismiss: false
        };

        this.showPopup$.next({...toast});
    }
}
