import {Injectable} from '@angular/core';
import {AuthResponse, AuthService} from './auth.service';
import {environment} from '../../environments/environment';
import {SessionStore, SessionState} from "../store/session/session.store";
import {SessionQuery} from "../store/session/session.query";
import {User} from "../component/common/interface";
import {firstValueFrom, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AlertService} from "./alert.service";
import {RegisterData} from "../page/register/register.page";
import {FidelityService} from "./fidelity.service";

@Injectable({providedIn: 'root'})
export class SessionService {
    protected constructor(protected  store: SessionStore, protected  query: SessionQuery, private alertService: AlertService,
                          protected  auth: AuthService, private http: HttpClient, private fidelityService: FidelityService) {
    }

    getUtente(id:string, cb: (resp: User) => void) {
        const url = environment.baseUrl + '/user/' + id;

      firstValueFrom(this.http.get<User>(url)).then(user => {
        this.store.update({user: user});

        if(user && user.ruolo) {
          this.store.update({admin: user.ruolo === 'ADMIN'});
        }

        cb(user);
      })
    }

  async login(username: string, password: string, rememberMe: boolean): Promise<AuthResponse> {
    return new Promise<AuthResponse>((resolve, reject) => {
      this.auth.getStore().reset();
      firstValueFrom(this.auth.authenticate({username, password}))
        .then(resp => {
          if(resp && resp.code === 401){
            const header = resp && resp.error && resp.error.title ? resp.error.title : 'error';
            const msg = resp && resp.error && resp.error.detail ? resp.error.detail : 'errore nel login';
            this.alertService.presentToast(header, msg, 'danger');

            this.logout();

            reject(resp.error);
          }
          if (!!resp && !!resp.id_token &&!! resp.data && resp.code === 200) {
            const id= resp.data.id as string;
            this.store.update({id: id, token: resp.id_token, rememberMe: true});
            this.getUtente(id,user => {
              this.auth.getStore().update({
                utente: user.id,
                nome: user.nome,
                access_token: resp.id_token,
                loginData: {username, password, rememberMe: true},
                rememberMe: true
              });
              resolve(resp); // Now resolving with the response object
            });
          } else {
            reject(new Error("No id_token found in response"));
          }
        }).catch(resp => {
        console.log('error', resp)
        const header = resp && resp.error && resp.error.title ? resp.error.title : 'error';
        const msg = resp && resp.error && resp.error.detail ? resp.error.detail : 'errore nel login';
        this.alertService.presentToast(header, msg, 'danger');

        this.logout();

        reject(resp.error);
      });
    });
  }

  logout(): void {
      this.auth.getStore().reset();
      this.store.reset()
      this.fidelityService.getStore().reset();
  }

  loggedUser():{admin?: boolean, logged: boolean} {
    const session = this.store.getValue()

    if(session && session.user){
      return {
        admin: session.admin,
        logged: true
      }
    }

    return {
      logged: false
    }
  }

  signIn(data: RegisterData){
      const url = environment.baseUrl + '/auth/register';
      return new Promise((resolve, reject) => {
        firstValueFrom(this.http.post<{data: User, token: string }>(url, data)).then(resp =>{
          if(resp && resp.data && resp.token){
            const user = resp.data
            this.store.update({user: user, token: resp.token, rememberMe: true});
            if(user && user.ruolo) {
              this.store.update({admin: user.ruolo === 'ADMIN'});
            }
            this.auth.getStore().update({
              utente: user.id,
              nome: user.nome,
              access_token: resp.token,
              loginData: {
                username: data.username,
                password: data.password,
                rememberMe: true
              },
              rememberMe: true
            });
            resolve(true);
          }else{
            this.alertService.presentToast('Errore', 'Errore durante la registrazione', 'danger').then();
            reject(false);
          }
        }).catch(err => {
          this.alertService.presentToast('Errore', 'Errore durante la registrazione', 'danger').then();
          reject(err);
        });
      });
  }

  getStore = () => this.store;
  getQuery = () => this.query;
}
