import {Injectable} from '@angular/core';
import {AuthQuery} from '../store/auth/auth.query';
import {AuthStore, AuthState, LoginData} from '../store/auth/auth.store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthBaseService} from "../store/auth/auth-base.service";
import {SessionStore} from "../store/session/session.store";
import {Observable} from 'rxjs/internal/Observable';
import {User} from "../component/common/interface";


export interface AuthResponse {
  id_token?: string;
  id: string;
  code: number;
  error?: {
    title: string;
    detail: string;
  };
  data: User
}

@Injectable({providedIn: 'root'})
export class AuthService extends AuthBaseService{
  protected constructor(
    protected override store: AuthStore,
    protected override query: AuthQuery,
    private http: HttpClient,
    // private alertService: AlertService,
    private sessionStore: SessionStore,) {
    super(store, query);
  }


  authenticate(loginData: LoginData): Observable<AuthResponse> {
    const request = this.getAuthenticateParameters(loginData);

    return this.http.post<AuthResponse>(request.url, request.params)
  }


  getAuthenticateParameters(loginData: LoginData): { url: string, params: any} {
    const url = this.getUrl() + '/auth/login';

    return {
      url,
      params: loginData,
    };
  }

  logout() {
    this.store.update({access_token: null, rememberMe: null, loginData: null, utente: null, nome: null});
    this.sessionStore.update({user: null});
  }



}
