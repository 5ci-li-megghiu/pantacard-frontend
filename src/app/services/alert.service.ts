import { Injectable } from '@angular/core';
import {AlertController, ToastController} from '@ionic/angular';
import { Color } from '@ionic/core';
import {AlertButton} from '@ionic/core/dist/types/components/alert/alert-interface';
import { forkJoin } from 'rxjs';
import {Risposta} from '../component/common/interface';
import {EventsService} from './events.service';

@Injectable({providedIn: 'root'})
export class AlertService {

  constructor(private toastController: ToastController, private alertController: AlertController,
              private eventsService: EventsService) {
    this.eventsService.showToast$.subscribe(toast => {
      if (toast) { this.presentToast(toast.header, toast.message, toast.color, toast.position); }
    });

    this.eventsService.showPopup$.subscribe(popup => {
      if (popup) { this.presentPopup(popup.header, popup.message, popup.buttons); }
    });
  }

  async presentToast(header: string, message: string, color: Color = 'dark', position: 'top' | 'bottom' | 'middle' = 'top') {
    const toast = await this.toastController.create({
      header,
      message,
      position,
      color,
      duration: 5000
    });
    await toast.present();
  }

  async presentPopup(header: string, message: string, buttons?: AlertButton[], cssClass?: string) {
    const toast = await this.alertController.create({
      header,
      message,
      buttons: buttons ? buttons : ['Ok'],
      backdropDismiss: false,
      cssClass: cssClass ? cssClass : ''
    });
    await toast.present();
  }

  presentPopupFromResp(resp: Risposta, handler?: (value: any) => boolean | void | { [key: string]: any; }) {
    forkJoin({
      header: resp.title,
      message: resp.message,
      ok: 'ok'
    }).subscribe(
      ({ok, header, message}) => {
        const okButton = {text: ok, handler};
        this.presentPopup(header, message, [okButton]).then(value => {});
      });
  }
}
