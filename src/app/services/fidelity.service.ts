import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {environment} from '../../environments/environment';
import {Fidelity, User} from "../component/common/interface";
import {firstValueFrom, of} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AlertService} from "./alert.service";
import {FidelityStore} from "../store/fidelity/fidelity.store";
import {FidelityQuery} from "../store/fidelity/fidelity.query";
import {ID} from "@datorama/akita";
import {HttpService} from "./http.service";
import * as moment from "moment/moment";
import {AppService} from "./app.service";

@Injectable({providedIn: 'root'})
export class FidelityService {
  try: number = 0
  protected constructor(protected  store: FidelityStore, protected  query: FidelityQuery, private appService: AppService,
                        private alertService: AlertService, protected  auth: AuthService, private http: HttpService) {
  }

  getFidelity(id: ID) {
      const url = environment.baseUrl + `/fidelity/${id}`;
      moment.locale('it')

      firstValueFrom(this.http.get<Fidelity>(url)).then(fidelity => {
        if(fidelity.dataInizio){fidelity.dataInizio = moment(fidelity.dataInizio).format('DD/MM/YYYY')}
        if(fidelity.dataFine){fidelity.dataFine = moment(fidelity.dataFine).format('DD/MM/YYYY')}

        if(fidelity.punti && fidelity.punti.length > 0){
          for(let i = 0; i < fidelity.punti.length; i++){
            fidelity.punti[i].ts = moment(fidelity.punti[i].ts).format('lll')
          }
        }

        this.store.update({activeFidelity: fidelity});
      }).catch((err : Error) => {
        if(this.try === 0){
          this.try++;
          this.getFidelity(id)
        }else{
          this.try = 0
          this.appService.getStore().update({rqActive: true})
          if(err.message === 'no elements in sequence'){
            err.message = 'Qualcosa é andato storto con il server'
          }
          this.alertService.presentToast('ERRORE', err.message, 'danger').then();
        }
      });;
  }

  getUserList() {
      const url = environment.baseUrl + `/user-list`;
      return firstValueFrom(this.http.get<User[]>(url));
  }

  getPointByCodice(codice: string) {
      const url = environment.baseUrl + `/point/${codice}`;
      // fai per codice
      return firstValueFrom(this.http.get<{n_punti: number}>(url));
  }

  modifyPoint(id: ID, n_punti: number) {
    const url = environment.baseUrl + `/set-point-manually`;
    const body = {
      id,
      punti: n_punti
    }
    return firstValueFrom(this.http.post(url, body));
  }

  aggiungiPunto(codice: string, punti: number, spesa: number) {
    const url = environment.baseUrl + `/punti/add-point`;
    const body = {
      codice,
      punti,
      spesa
    }
    return firstValueFrom(this.http.post(url, body));
  }

  resetFidelity(codice: string) {
      const url = environment.baseUrl + `/complete-fidelity/` + codice;
      return firstValueFrom(this.http.get(url));
  }



  getStore = () => this.store;
  getQuery = () => this.query;
}
