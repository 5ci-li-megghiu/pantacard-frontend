import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import {SessionService} from "./session.service";

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private session: SessionService) {
  }
  canActivate() {
    const user = this.session.loggedUser();
    return user.logged && user.admin!
  }

}
