import { Injectable } from '@angular/core';
import {BaseAkitaService} from '../store/base/BaseAkitaService';
import { DeviceDetectorService } from 'ngx-device-detector';
import { HttpParams, HttpClient } from '@angular/common/http';
import {AppState, AppStore} from "../store/app/app.store";
import {AppQuery} from "../store/app/app.query";

@Injectable({ providedIn: 'root' })
export class AppService extends BaseAkitaService<AppState, AppStore, AppQuery> {

  url = '';

  constructor(
    protected override store: AppStore,
    protected override query: AppQuery,
    private deviceService: DeviceDetectorService,
    private httpClient: HttpClient) {
    super(store, query);
    this.getDevieceInfo();
  }

  public onStateChanged(state: AppState): void {}

  getDevieceInfo(): void {
    let deviceType = '';
    if (this.deviceService.isMobile()) { deviceType = 'mobile'; }
    if (this.deviceService.isTablet()) { deviceType = 'tablet'; }
    if (this.deviceService.isDesktop()) { deviceType = 'desktop'; }

    this.store.update({deviceInfo: this.deviceService.getDeviceInfo(), deviceType});
  }

  getAppStatus = (url: string, params?: HttpParams) => {
    if (!params) { params = new HttpParams(); }

    return this.httpClient.get(url, {params});
  }
}
