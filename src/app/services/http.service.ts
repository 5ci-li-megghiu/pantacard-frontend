import {
    HttpClient,
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpHeaders,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {of, Subject} from 'rxjs';
import {Observable} from 'rxjs/internal/Observable';
import {catchError, switchMap, tap} from 'rxjs/operators';
// Fix warning 'cannot call a namespace('moment')
// Con '* as momennt' da il warning di sopra in build
import * as moment from 'moment';
import { runStoreAction, StoreAction } from '@datorama/akita';
import {EventsService} from "./events.service";
import {AuthResponse, AuthService} from "./auth.service";
import {AppService} from "./app.service";

export const HEADER_KEYS = {
    // AUTH_TOKEN: 'X-Authentication-Token',
    // CALL_OUTCOME: 'X-Request-Outcome',
    PAGE_ONLY_TOTAL: true,
    CURRENT_PAGE: 'X-Pagination-Current-Page',
    CURRENT_PAGE_ELEMENTS: 'X-Pagination-Per-Page',
    TOTAL_ELEMENTS: 'X-Pagination-Total-Count',
    TOTAL_PAGES: 'X-Pagination-Page-Count',
    // PROFILATION: 'X-PROFILAZIONE',
    CSRF: 'X-XSRF-TOKEN'
};

export interface PaginationData {
    currentPage?: number;
    currentPageElements?: number;
    totalElements?: number;
    totalPages?: number;
}

export enum StatusResponse {
    SUCCESS = 200,
    CREATED = 201
}

export interface GenericResponse<T> {
    code: number;
    ok?: boolean;
    esito: string | null;
    messages?: any[] | null;
    pagination?: PaginationData;
    data?: T;
    status?: number;
    response?: HttpResponse<any>;
}

export interface MyHeader {
  key: string;
  value: string;
}

@Injectable({providedIn: 'root'})
export class HttpService implements HttpInterceptor {
    moment = moment;

    countRequests = 0;
    protected refreshSubject: Subject<any> = new Subject<any>();

    constructor(protected http: HttpClient, protected auth: AuthService, protected appService: AppService,
                protected eventsService: EventsService) {
    }

    protected static _staticRewrapErrorCallResponse(error: HttpErrorResponse): GenericResponse<any> | null {
        if (error.error) {
            // console.error(`Backend returned code ${error.status}, body was: `, error.error);
            return {
                code: 1,
                ok: false,
                esito: 'ERROR',
                messages: [
                    {
                        type: 'ERROR',
                        title: '',
                        text: `Backend returned code ${error.status}, body was: ${error.error}`
                    }
                ]
            };
        }
        return null;
    }

    getToken = () => {
        const data = this.auth.getStore().getValue();
        return data.access_token ? data.access_token : null;
    }
    getBodyWithContract(data: any): any {
        const contract = this.auth.getStore().getValue().contract;
        return contract ? {...data, contract} : data;
    }
    public get = <T>(url: string, options: any = {}): Observable<T> => {
        // @ts-ignore
        return this.http.get<T>(url, this.prepareOptions(options));
    }
    doGet = <T>(url: string, options: any = {},
                callback: (value: GenericResponse<T>, resp: HttpResponse<T>) => any,
                callbackErr: (err: any) => any = this.defaultErrorCallback): Observable<HttpResponse<T>> => {
        let pag = 0;
        let size = 0;

        if (HEADER_KEYS.PAGE_ONLY_TOTAL && options && options.params && options.params.get) {
            pag = parseInt(options.params.get('page'), 10);
            size = parseInt(options.params.get('size'), 10);
        }

        return this.get(
            url,
            {
                ...options,
                observe: 'response'
            })
          // @ts-ignore
            .pipe(tap((value: HttpResponse<T>) => {
                callback(this.rewrapCallResponse<T>(value, pag, size), value);
            }, (error) => {
                this.eventsService.presentToast(error.name, error.message, 'danger', 'top');
                callbackErr(error);
            }));
    }

    doGetAuto = <T>(url: string, options: any = {},
                    callback: (value: GenericResponse<T>, resp: HttpResponse<T>) => any,
                    callbackErr: (err: any) => any = this.defaultErrorCallback): void => {
        this.doGet(url, options, callback, callbackErr)
            .subscribe();
    }

    post = <T>(url: string, body: any, options: any = {}): Observable<T> => {
        body = this.getBodyWithContract(body);

        options = this.prepareOptions(options);

        if (!options.headers) { options.headers = new HttpHeaders(); }
        options.headers = options.headers.append('Content-Type', 'application/json');

        // in caso di 'Content-Type', 'application/x-www-form-urlencoded' devo passare il body come HttpService.JSON_to_URLEncoded(body)
        // @ts-ignore
        return this.http.post<T>(url, body, options)
            .pipe(
                catchError(
                    resp => {
                        if (resp && resp.status && resp.status === 422 && resp.error.length) {
                            const error = resp.error[0];
                            this.eventsService.presentPopup(error, error.message);
                        }
                        return of(resp);
                    })
            );
    }

    doPost = <T>(url: string, body: any, options: any = {},
                 callback: (value: GenericResponse<T>, resp: HttpResponse<T>) => any,
                 callbackErr: (err: any) => any = this.defaultErrorCallback): Observable<HttpResponse<T>> => {

        return this.post(
            url,
            body,
            {
                ...options,
                observe: 'response'
            })
          // @ts-ignore
            .pipe(tap((value: HttpResponse<T>) => {
                callback(this.rewrapCallResponse<T>(value), value);
            }, (error) => {
                this.eventsService.presentToast(error.name, error.message, 'danger', 'top');

                callbackErr(error);
            }));
    }

    doPostAuto = <T>(url: string, body: any, options: any = {},
                     callback: (value: GenericResponse<T>, resp: HttpResponse<T>) => any,
                     callbackErr: (err: any) => any = this.defaultErrorCallback): void => {
        this.doPost(url, body, options, callback, callbackErr).subscribe();
    }

    put = <T>(url: string, body: any, options: any = {}): Observable<T> => {
        body = this.getBodyWithContract(body);

        options = this.prepareOptions(options);
        if (!options.headers) {
            options.headers = new HttpHeaders();
        }
        options.headers = options.headers.append('Content-Type', 'application/json');

        // @ts-ignore
        return this.http.put<T>(url, body, options);
    }

    doPut = <T>(url: string, body: any, options: any = {},
                callback: (value: GenericResponse<T>, resp: HttpResponse<T>) => any,
                callbackErr: (err: any) => any = this.defaultErrorCallback): Observable<HttpResponse<T>> => {

        return this.put(
            url,
            body,
            {
                ...options,
                observe: 'response'
            })
          // @ts-ignore
            .pipe(tap((value: HttpResponse<T>) => {
                callback(this.rewrapCallResponse<T>(value), value);
            }, (error) => {
                this.eventsService.presentToast(error.name, error.message, 'danger', 'top');

                callbackErr(error);
            }));
    }

    doPutAuto = <T>(url: string, body: any, options: any = {},
                    callback: (value: GenericResponse<T>, resp: HttpResponse<T>) => any,
                    callbackErr: (err: any) => any = this.defaultErrorCallback): void => {
        this.doPut(url, body, options, callback, callbackErr)
            .subscribe();
    }

    delete = <T>(url: string, options: any = {}): Observable<T> => {
        // @ts-ignore
        return this.http.delete<T>(url, this.prepareOptions(options));
    }

    doDelete = <T>(url: string, options: any = {}, paramObj: any,
                   callback: (value: GenericResponse<T>, resp: HttpResponse<T>) => any,
                   callbackErr: (err: any) => any = this.defaultErrorCallback): Observable<HttpResponse<T>> => {

        return this.delete(
            url,
            {
                ...options,
                observe: 'response'
            })
          // @ts-ignore
            .pipe(tap((value: HttpResponse<T>) => {
                callback(this.rewrapCallResponse<T>(value), value);
            }, (error) => {
                this.eventsService.presentToast(error.name, error.message, 'danger', 'top');
                callbackErr(error);
            }));
    }

    doDeleteAuto = <T>(url: string, body: any, options: any = {},
                       callback: (value: GenericResponse<T>, resp: HttpResponse<T>) => any,
                       callbackErr: (err: any) => any = this.defaultErrorCallback): void => {
        this.doDelete(url, body, options, callback, callbackErr)
            .subscribe();
    }

    public prepareOptions(options: any, observe: 'response' | 'body' = 'body'): any {

        return {
            responseType: 'json',
            observe,
            ...options,
            headers: this.prepareHeader(options.headers)
        };
    }

    public prepareHeader = (headers: HttpHeaders): HttpHeaders => {
        if (!headers) {
            headers = new HttpHeaders();
        }
        return this.addAuthToken(headers);
    }

    getPaginationData(response: HttpResponse<any>, page?: number, size?: number): PaginationData {
        const pag: PaginationData = {};

        if (response.headers.has(HEADER_KEYS.CURRENT_PAGE)) {
            const currentPage = response.headers.get(HEADER_KEYS.CURRENT_PAGE);
            pag.currentPage = currentPage !== null ? parseInt(currentPage, 10) : 1;
        } else if (page) {
            pag.currentPage = page;
        }
        if (response.headers.has(HEADER_KEYS.CURRENT_PAGE_ELEMENTS)) {
            const totalPageElements = response.headers.get(HEADER_KEYS.CURRENT_PAGE_ELEMENTS);
            pag.currentPageElements = totalPageElements !== null ? parseInt(totalPageElements, 10) : 1;
        } else if (size) {
            pag.currentPageElements = size;
        }
        if (response.headers.has(HEADER_KEYS.TOTAL_ELEMENTS)) {
          const totalElements = response.headers.get(HEADER_KEYS.TOTAL_ELEMENTS);
            pag.totalElements = totalElements !== null ? parseInt(totalElements, 10) : 1;
        }
      let cpe = pag.currentPageElements;
      if (cpe === undefined) { cpe = 1; }
      let te = pag.totalElements;
      if (te === undefined) { te = 1; }
        if (response.headers.has(HEADER_KEYS.TOTAL_PAGES)) {
            const totalPages = response.headers.get(HEADER_KEYS.TOTAL_PAGES);
            pag.totalPages = totalPages !== null ? parseInt(totalPages, 10) : 1;
        } else if (cpe > 0 && te > 0) {
            pag.totalPages = te / cpe;
        }
        return pag;
    }

    public rewrapCallResponse = <T>(response: HttpResponse<T>, page?: number, size?: number): GenericResponse<T> => {

        return {
            code: 0,
            ...response,
            data: response.body,
            esito: null,
            messages: null,
            pagination: this.getPaginationData(response, page, size)
        } as GenericResponse<T>;
    }

    public defaultErrorCallback = (error: HttpErrorResponse) => {
        HttpService._staticRewrapErrorCallResponse(error);
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.match('^((http|https)?:\\/\\/).*')) {
            const started = Date.now();
            this.countRequests += 1;

            setTimeout(() => this.appService.getStore()
                .update({rqActive: true}));

            let reloginAttemp = false;

            return next.handle(req)
                .pipe(
                    catchError(
                        resp => {

                            this.countRequests -= 1;

                            if (this.countRequests <= 0) {
                                this.countRequests = 0;

                                setTimeout(() => this.appService.getStore()
                                    .update({rqActive: false}));
                            }

                            if (resp && resp.status && resp.status === 500) {
                                // errore server
                                this.eventsService.presentToast('Server error', '', 'danger');
                            } else if (resp && resp.status && resp.status === 422 && resp.error.length) {
                                // errore di validazione
                                const error = resp.error[0];
                                this.eventsService.presentPopup(error, error.message);
                            } else if (resp && (resp.status === 401)) {
                                let expiredToken = this.auth.getQuery().getValue().expire_token;
                                if (!expiredToken) {
                                  expiredToken = 0;
                                }
                                if (expiredToken < Date.now()) {
                                    if (!reloginAttemp) {
                                        reloginAttemp = true;
                                        return this.refreshToken()
                                            .pipe(
                                                switchMap(() => {
                                                    return next.handle(this.updateHeader(req));
                                                })
                                            ) as Observable<HttpEvent<any>>;
                                    } else {
                                        // logg('tentativo di relogin già effettuato');
                                    }
                                } else {
                                    // logg('token non ancora scaduto');
                                }
                            }
                            return of(resp);
                        }),
                    tap(
                        (resp) => {
                            if (resp instanceof HttpResponse) {
                                const elapsed = (Date.now() - started) / 1000;
                                // logg(`${elapsed}s`, req.method, req.url);

                                this.countRequests -= 1;

                                if (this.countRequests <= 0) {
                                    this.countRequests = 0;
                                    setTimeout(() =>
                                        runStoreAction('app', StoreAction.Update, update => update({rqActive: false}))
                                    );
                                }
                            }
                        })
                );
        }
        return next.handle(req);
    }

    updateHeader(req: any) {
        const authToken = this.auth.getQuery().getValue().access_token;
        return req.clone({headers: req.headers.set('Authorization', `Bearer ${authToken}`)});
    }

    protected refreshToken(): any {
        this.refreshSubject.subscribe({complete: () => this.refreshSubject = new Subject<any>()});
        if (this.refreshSubject.observers.length === 1) {
            const loginData = this.auth.getQuery().getValue().loginData;
            if (loginData) {
                const p = this.auth.getAuthenticateParameters(this.auth.getQuery().getValue().loginData!);
                this.http.post<AuthResponse>(p.url, p.params)
                    .pipe(tap(res => {
                        // @ts-ignore
                        this.auth.getStore().update({access_token: res.id_token});
                    }))
                    .subscribe(this.refreshSubject);
            }
        }
        return this.refreshSubject;
    }

    protected addAuthToken = (header: HttpHeaders): HttpHeaders => {
        const token = this.getToken();
        console.log('token', token)
        if (token) {
            header = header.append('Authorization', `Bearer ${token}`);
        }
        return header;
    }
}
