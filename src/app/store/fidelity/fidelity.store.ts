import { Store, StoreConfig } from '@datorama/akita';
import {Injectable} from "@angular/core";
import {Fidelity} from "../../component/common/interface";

export interface FidelityState {
  activeFidelity?: Fidelity
}

export function createInitialState(): FidelityState {
  return {};
}

@Injectable({providedIn: 'root'})
@StoreConfig({ name: 'fidelity' })
export class FidelityStore extends Store<FidelityState> {
  constructor() {
    super(createInitialState());
  }
}

