import { Injectable } from '@angular/core';
import {BaseQuery} from '../base/BaseQuery';
import {FidelityState, FidelityStore} from "./fidelity.store";

@Injectable({ providedIn: 'root' })
export class FidelityQuery extends BaseQuery<FidelityState, FidelityStore> {

  constructor(protected override store: FidelityStore) {
    super(store);
  }

  fidelity$ = this.select(('activeFidelity'));
}
