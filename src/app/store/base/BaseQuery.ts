import {Query} from '@datorama/akita';
import {BaseStore} from './BaseStore';

export abstract class BaseQuery<State, _Store extends BaseStore<State>> extends Query<State> {

  protected constructor(protected override store: _Store) {
    super(store);
  }
}
