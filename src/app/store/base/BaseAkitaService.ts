import {Subscription} from 'rxjs';
import {BaseQuery} from './BaseQuery';
import {BaseStore} from './BaseStore';
import {environment} from '../../../environments/environment';

// Ho creato questa classe base per necessità di non importare httpService (vedi authService)
export abstract class BaseAkitaService<_State, _Store extends BaseStore<_State>, _Query extends BaseQuery<_State, _Store>> {

  private querySub: Subscription | null;
  protected abstract url: string;

  protected environment;

  public abstract onStateChanged(state: _State): void;

  protected constructor(
    protected store: _Store,
    protected query: _Query) {
    setTimeout(this.subscribeQuery);

    this.environment = environment;
    this.querySub = null;
  }

  subscribeQuery = () => {
    this.unsubscribeQuery();
    this.querySub = this.getQuery()
      .select()
      .subscribe((state) => {
        this.onStateChanged(state);
      });
  };

  unsubscribeQuery = () => {
    if (this.querySub) {
      this.querySub.unsubscribe();
      this.querySub = null;
    }
  };

  protected getUrl = () => this.environment.baseUrl;

  getStore = () => this.store;
  getQuery = () => this.query;
}
