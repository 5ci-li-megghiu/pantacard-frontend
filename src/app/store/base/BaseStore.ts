import {Store} from '@datorama/akita';
export abstract class BaseStore<State> extends Store {

  protected constructor(initialState: State) {
    super(initialState as Partial<any>);
  }
}
