import {BaseAkitaService} from './BaseAkitaService';
import {BaseQuery} from './BaseQuery';
import {BaseStore} from './BaseStore';
import {HttpService} from '../../services/http.service';

export abstract class BaseService<_State, _Store extends BaseStore<_State>, _Query extends BaseQuery<_State, _Store>>
  extends BaseAkitaService<_State, _Store, _Query> {

  protected constructor(
    protected http: HttpService,
    protected override store: _Store,
    protected override query: _Query) {
    super(store, query);
  }
}
