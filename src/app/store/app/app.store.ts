import { Store, StoreConfig } from '@datorama/akita';
import {Injectable} from "@angular/core";

export interface AppState {
  rqActive: boolean;
  deviceInfo?: {
    userAgent: string;
    os: string;
    browser: string;
    device: string;
    os_version: string;
    browser_version: string;
  };
  deviceType?: string;
  orientation: 'portrait' | 'landscape';
}

export function createInitialState(): AppState {
  return {
    rqActive: false,
    orientation: window.innerHeight > window.innerWidth ? 'portrait' : 'landscape'
  };
}

@Injectable({providedIn: 'root'})
@StoreConfig({ name: 'app' })
export class AppStore extends Store<AppState> {
  constructor() {
    super(createInitialState());
  }
}

