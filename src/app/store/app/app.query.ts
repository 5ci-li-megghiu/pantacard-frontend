import { Injectable } from '@angular/core';
import {BaseQuery} from '../base/BaseQuery';
import { AppStore, AppState } from './app.store';

@Injectable({ providedIn: 'root' })
export class AppQuery extends BaseQuery<AppState, AppStore> {

  constructor(protected override store: AppStore) {
    super(store);
  }
}
