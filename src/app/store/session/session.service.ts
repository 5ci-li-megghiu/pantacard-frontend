import {Injectable} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {SessionQuery} from './session.query';
import { SessionStore, SessionState } from './session.store';
import {BaseService} from '../base/BaseService';
import {GenericResponse, HttpService} from '../../services/http.service';
import {User} from '../../component/common/interface';

@Injectable({ providedIn: 'root' })
export abstract class SessionBaseService extends BaseService<SessionState, SessionStore, SessionQuery> {

  public url = 'account';

  protected constructor(protected override http: HttpService, protected override store: SessionStore, protected override query: SessionQuery,
                        protected auth: AuthService
  ) {
    super(http, store, query);
    this.auth.getQuery().select('access_token').subscribe((token) => {
        this.store.update({token});
    });
  }

  // Non rimuovere questa funzione, creerebbe un errore!
  onStateChanged(state: SessionState): void {}

  /*public getUtente(cb: (resp: GenericResponse<User>) => void): void {
    this.http.doGetAuto<User>( `${this.getUrl()}`, {}, (resp) => {
      cb(resp);
      const user = resp.data;
      this.store.update({user});
      this.translate.get('welcome_nome', {nome: resp.data.firstName + ' ' + resp.data.lastName}).toPromise().then(title => {
        this.eventsService.presentToast(title, '', 'success');
      });
    }, (err) => {
      this.logout();
    });
  }*/
  public abstract getUtente(cb: (resp: GenericResponse<User>) => void): void;

  /*public onStateChanged(state: SessionState): void {
    if (state.token && this.http && (state.user == null || (state.user && state.user.authorities && state.user.authorities.length && !state.user.authorities.find(role => role === 'ROLE_ADMIN')))) {
      this.getUtente((resp) => {
        this.unsubscribeQuery();

        const user = resp.data;

        if (user && user.authorities && user.authorities.length && user.authorities.find(role => role === 'ROLE_ADMIN')) {
          this.store.login({...resp.data, contract: null});
        } else {
          this.store.login(user);
        }
      });
      /!*this.http.doGetAuto( `${this.getUrl()}/get`, {}, (resp) => {});*!/
    }
  }*/

  /*login = async (username: string, password: string, rememberMe: boolean = false) => {
    this.auth.getStore().reset();
    await this.auth.authenticate({ username, password, rememberMe });
  }*/

  abstract login(username: string, password: string, rememberMe: boolean): void;

  /*logout = () => {
    this.getStore().logout();
    this.getStore().reset();
    this.auth.getStore().reset();
    // this.contrattiService.getStore().reset();
    this.subscribeQuery();
  }*/
  abstract logout(): void;
}
