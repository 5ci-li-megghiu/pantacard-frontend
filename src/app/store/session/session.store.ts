import { Injectable } from '@angular/core';
import { StoreConfig } from '@datorama/akita';
import {BaseStore} from '../base/BaseStore';
import {User} from '../../component/common/interface';

export interface SessionState {
  token: string | null;
  user: User | null;
  id?: string;
  rememberMe?: boolean;
  admin?: boolean;

  extra?: { [key: string]: any };
}

function createInitialState(): SessionState {
  return {
    token: null,
    user: null,
    rememberMe: false
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'session', resettable: true })
export class SessionStore extends BaseStore<SessionState> {

  constructor() {
    super(createInitialState());
  }

  login = (user: User) => {
    this.update({ user });
  }

  logout = () => this.update(createInitialState());
}

