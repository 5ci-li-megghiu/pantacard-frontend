import { Injectable } from '@angular/core';
import {toBoolean} from '@datorama/akita';
import {filter, map} from 'rxjs/operators';
import { SessionStore, SessionState } from './session.store';
import {BaseQuery} from '../base/BaseQuery';

@Injectable({ providedIn: 'root' })
export class SessionQuery extends BaseQuery<SessionState, SessionStore> {

  constructor(protected override store: SessionStore) {
    super(store);
  }

  isLoggedIn$ = this.select(({user}) => toBoolean(user));

  loggedInUser$ = this.select().pipe(
    filter((state) => toBoolean(state.user)),
    map(({user}) => `${user!.nome} ${user!.cognome}`)
  );

  isAdmin$ = this.select(({admin}) => toBoolean(admin));

  user$ = this.select(({user}) => user);
}
