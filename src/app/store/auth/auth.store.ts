import { Injectable } from '@angular/core';
import { StoreConfig } from '@datorama/akita';
import {BaseStore} from '../base/BaseStore';

export interface LoginData {
  username: string;
  password: string;
  rememberMe?: boolean;
}

export interface AuthState {
  utente?: number;
  nome?: string;
  access_token?: string;
  expire_token?: number;
  loginData?: LoginData;
  rememberMe?: boolean;
}

function createInitialState(): AuthState {
  return {
    access_token: undefined,
    expire_token: undefined,
    rememberMe: false,
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'auth', resettable: true })
export class AuthStore extends BaseStore<AuthState> {

  constructor() {
    super(createInitialState());
  }

  remove = () => {
    this.update(createInitialState());
  }
}
