import {Injectable} from '@angular/core';
import {AuthQuery} from './auth.query';
import {AuthState, AuthStore, LoginData} from './auth.store';
import {BaseAkitaService} from '../base/BaseAkitaService';
import {Observable} from 'rxjs/internal/Observable';

export interface AuthResponse {
  id_token?: string;
}

@Injectable({ providedIn: 'root' })
export abstract class AuthBaseService extends BaseAkitaService<AuthState, AuthStore, AuthQuery> {

  public url = '';

  protected constructor(protected override store: AuthStore, protected override query: AuthQuery) {
    super(store, query);
  }

  // Non rimuovere questa funzione, creerebbe un errore!
  onStateChanged(state: AuthState): void {}

  abstract authenticate(loginData: LoginData): Observable<AuthResponse>;

  abstract getAuthenticateParameters(loginData?: LoginData): any;
}
