import { Injectable } from '@angular/core';
import { AuthStore, AuthState } from './auth.store';
import {BaseQuery} from '../base/BaseQuery';

@Injectable({ providedIn: 'root' })
export class AuthQuery extends BaseQuery<AuthState, AuthStore> {

  constructor(protected override store: AuthStore) {
    super(store);
  }
}
